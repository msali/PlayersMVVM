//
//  Coordinator.swift
//  PlayersMVVM
//
//  Created by Salierno Mario on 14/02/21.
//

import Foundation
import UIKit

protocol Coordinator {
    
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    func start()
    
}

class MainCoordinator: Coordinator {
    
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []
    
    init(navController: UINavigationController) {
        navigationController = navController
    }
    
    func start() {
        let vc = TeamsViewController()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func openTeamPlayers(team: TeamModel){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Players", bundle:nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "PlayersViewController") as? PlayersViewController else { return }
        vc.coordinator = self
        vc.team = team
        navigationController.pushViewController(vc, animated: true)
    }
    
    func openPlayerDetail(player: PlayerModel){
        let storyBoard : UIStoryboard = UIStoryboard(name: "PlayerDetail", bundle:nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "PlayerDetailViewController") as? PlayerDetailViewController else { return }
        vc.coordinator = self
        vc.player = player
        navigationController.pushViewController(vc, animated: true)
    }
}
