//
//  PlayersViewController.swift
//  PlayersMVVM
//
//  Created by Salierno Mario on 14/02/21.
//

import Foundation
import UIKit

class PlayersViewController : UIViewController {
    
    var team: TeamModel?
    
    var playersViewModel = PlayersViewModel()
    
    weak var coordinator: MainCoordinator?
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = true
        
        playersViewModel.fetchPlayers{ [weak self] teams in
            DispatchQueue.main.async {
                self?.updateUI()
            }
        }
    }
    
    func updateUI() {
        tableView.reloadData()
    }
    
}
extension PlayersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let teamsData = playersViewModel.players?.data else { return 0 }
        return teamsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let playerData = playersViewModel.players?.data,
              let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath) as? PlayerCell
        else { return UITableViewCell() }
        
        let cellData = playerData[indexPath.row]
        cell.nameLabel.text = (cellData.first_name ?? "No Name") + " " + (cellData.last_name ?? "No Lastname")
        
        return cell
    }
}

extension PlayersViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let playerData = playersViewModel.players?.data else { return }
        let cellData = playerData[indexPath.row]
        
        coordinator?.openPlayerDetail(player: cellData)
    }
}
