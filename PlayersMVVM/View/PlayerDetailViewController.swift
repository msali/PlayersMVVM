//
//  PlayerDetailViewController.swift
//  PlayersMVVM
//
//  Created by Salierno Mario on 14/02/21.
//

import Foundation
import UIKit

class PlayerDetailViewController : UIViewController {
 
    weak var coordinator: MainCoordinator?
    
    var player: PlayerModel?
    
    @IBOutlet var nameLbl: UILabel!
    
    @IBOutlet var position: UILabel!
    
    @IBOutlet var team: UILabel!
    
    @IBOutlet var sizeLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var nameStr: String = "Name: " + (player?.first_name ?? "No Name")
        nameStr += " "
        nameStr += (player?.last_name ?? "No last name")
        nameLbl.text = nameStr
        position.text = "Position: " + (player?.position ?? "no position")
        team.text = player?.team?.full_name
        
        var sizeStr: String = "H inches: \(player?.height_inches ?? 0)"
        sizeStr += "\t H feet: \(player?.height_feet ?? 0)"
        sizeStr += "\t W pounds: \(player?.weight_pounds ?? 0)"
        sizeLbl.text = sizeStr
        
    }
    
}
