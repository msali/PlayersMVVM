//
//  ViewController.swift
//
//  Created by Salierno Mario on 13/02/21.
//
//

import UIKit

class TeamsViewController: UIViewController {
    
    var tableView = UITableView()
    
    var teamsViewModel = TeamsViewModel()
    
    weak var coordinator: MainCoordinator?


    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = true
        
        self.view.addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ])
        
        
        teamsViewModel.fetchTeams{ [weak self] teams in
            DispatchQueue.main.async {
                self?.updateUI()
            }
        }
    }
    
    func updateUI() {
        tableView.reloadData()
    }
}

extension TeamsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let teamsData = teamsViewModel.teams?.data else { return 0 }
        return teamsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let teamsData = teamsViewModel.teams?.data else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let cellData = teamsData[indexPath.row]
        cell.textLabel?.text = cellData.full_name
        
        return cell
    }
}

extension TeamsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let teamsData = teamsViewModel.teams?.data else { return }
        let cellData = teamsData[indexPath.row]
        
        coordinator?.openTeamPlayers(team: cellData)
    }
}

