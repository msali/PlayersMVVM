//
//  APIManager.swift
//
//  Created by Salierno Mario on 13/02/21.
//
//

import Foundation

class APIManager {
    static let shared: APIManager = APIManager()

    enum APIError: Error {
        case invalidURL
        case invalidResponse(Data?, URLResponse?)
    }
    
    public func get(urlString: String, completionBlock: @escaping (Result<Data, Error>) -> Void) {
        
        guard let url = URL(string: urlString) else {
            completionBlock(.failure(APIError.invalidURL))
            return
        }
        
        let headers = [
            "x-rapidapi-key": rapidapiKey,
            "x-rapidapi-host": rapidapiHost
        ]

        let request = NSMutableURLRequest(url: url,  cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                completionBlock(.failure(error!))
                return
            }

            guard
                let responseData = data,
                let httpResponse = response as? HTTPURLResponse,
                200 ..< 300 ~= httpResponse.statusCode else {
                    completionBlock(.failure(APIError.invalidResponse(data, response)))
                    return
            }

            //print(String(data: responseData as? Data ?? Data(), encoding: String.Encoding.utf8) ?? "failed casting")
            completionBlock(.success(responseData))
        })

        dataTask.resume()
        
    }
}
