//
//  PlayersDataModel.swift
//  PlayersMVVM
//
//  Created by Salierno Mario on 15/02/21.
//

import Foundation

class PlayersDataModel: NSObject, Codable {
    let data: [PlayerModel]?
    let meta: Meta?
}

class PlayerModel: NSObject, Codable {
    let id, height_feet, height_inches, weight_pounds: Int?
    let first_name, last_name, position: String?

    let team: TeamModel?
    
    enum CodingKeys: String, CodingKey {
        case id, height_feet, height_inches, weight_pounds, first_name, last_name, position, team
        
    }
}
