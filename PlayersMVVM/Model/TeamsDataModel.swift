//
//  ContentModel.swift
//
//  Created by Salierno Mario on 13/02/21.
//
//

import Foundation


class TeamsDataModel: NSObject, Codable {
    let data: [TeamModel]?
    let meta: Meta?
}

class TeamModel: NSObject, Codable {
    let id: Int?
    let abbreviation, city, conference, division, full_name, name: String?

    enum CodingKeys: String, CodingKey {
        case id, abbreviation, city, conference, division, full_name, name
        
    }
}


class Meta: NSObject, Codable {
    let total_pages: Int?
    let current_page: Int?
    let next_page: Int?
    let per_page: Int?
    let total_count: Int?

    enum CodingKeys: String, CodingKey {
        case total_pages, current_page, next_page, per_page, total_count
    }
}
