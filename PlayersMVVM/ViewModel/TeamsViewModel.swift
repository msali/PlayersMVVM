//
//  TeamsViewModel.swift
//
//  Created by Salierno Mario on 13/02/21.
//
//

import Foundation

class TeamsViewModel {
    
    init(model: TeamsDataModel? = nil) {
        if let inputModel = model {
            teams = inputModel
        }
    }
    var teams: TeamsDataModel?
}

extension TeamsViewModel {
    
    func fetchTeams(completion: @escaping (Result<TeamsDataModel, Error>) -> Void) {
        APIManager.shared.get(urlString: teamsAPI + "?page=0", completionBlock: { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .failure(let error):
                print ("failure", error)
            case .success(let dta) :
                let decoder = JSONDecoder()
                do
                {
                    print(dta)
                    self.teams = try decoder.decode(TeamsDataModel.self, from: dta)
                    completion(.success(try decoder.decode(TeamsDataModel.self, from: dta)))
                } catch {
                    print("DECODE ERROR")
                }
            }
        })
    }
}
