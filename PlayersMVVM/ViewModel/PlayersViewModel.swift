//
//  PlayersViewModel.swift
//  PlayersMVVM
//
//  Created by Salierno Mario on 15/02/21.
//

import Foundation

class PlayersViewModel {
    
    init(model: PlayersDataModel? = nil) {
        if let inputModel = model {
            players = inputModel
        }
    }
    var players: PlayersDataModel?
}

extension PlayersViewModel {
    
    func fetchPlayers(completion: @escaping (Result<PlayersDataModel, Error>) -> Void) {
        APIManager.shared.get(urlString: playersAPI, completionBlock: { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .failure(let error):
                print ("failure", error)
            case .success(let dta) :
                let decoder = JSONDecoder()
                do
                {
                    print(dta)
                    self.players = try decoder.decode(PlayersDataModel.self, from: dta)
                    completion(.success(try decoder.decode(PlayersDataModel.self, from: dta)))
                } catch {
                    print("DECODE ERROR")
                }
            }
        })
    }
}
