//
//  AppDelegate.swift
//  PlayersMVVM
//
//  Created by Salierno Mario on 13/02/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var coordinator: MainCoordinator?
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
            // Override point for customization after application launch.
        
            let navController = UINavigationController()
            coordinator = MainCoordinator(navController: navController)
        
            coordinator?.start()
        
            self.window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            return true
    }


}

